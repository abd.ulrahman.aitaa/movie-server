<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Genre;
use App\Http\Resources\GenreBaseResource;
use App\Http\Controllers\ResponseContoller;
use Illuminate\Support\Facades\Validator;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get All Genre
        // $genres = Genre::all();
        $genres = GenreBaseResource::collection(Genre::all());

        return (new ResponseContoller)->jsonResponse(true, '', $genres, 200, $genres->count());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        //Check if request fields validate
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
            ]
        );

        //Validate Fail
        if ($validator->fails()) {
            return (new ResponseContoller)->jsonError(false, 'Validation Error', 422, $validator->errors());
        }

        //Create new Genre in database
        $genre = Genre::create($request->all());

        if ($genre) {
            //if genre created successfully
            return (new ResponseContoller)->jsonResponse(true, 'Genre Created Successfully', $genre, 201);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Something Went Wrong!', [], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::find($id);
        if ($genre) {
            return (new ResponseContoller)->jsonResponse(true, '', $genre, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Genre Found!', [], 404);
        }
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        if ($genre) {
            return (new ResponseContoller)->jsonResponse(true, '', $genre, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Genre Found!', [], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check if request fields validate
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
            ]
        );

        //Validate Fail
        if ($validator->fails()) {
            return (new ResponseContoller)->jsonError(false, 'Validation Error', 422, $validator->errors());
        }

        //find the genre by id
        $genre = Genre::find($id);

        if ($genre) {
            //if genre found
            //Update Genre in database
            $genre->update($request->all());
            return (new ResponseContoller)->jsonResponse(true, 'Genre Updated Successfully', $genre, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Genre Found!', [], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::find($id);

        if ($genre) {
            $genre->delete();
            return (new ResponseContoller)->jsonResponse(true, 'Genre Deleted Successfully', $genre, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Genre Found!', [], 404);
        }
    }
}
