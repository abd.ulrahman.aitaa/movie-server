<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\ResponseContoller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function createUser(Request $request)
    {
        try {
            //Check if request fields validate
            $validator = Validator::make(
                $request->all(),
                [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|string|min:8'
                ]
            );

            //Validate Fail
            if ($validator->fails()) {
                return (new ResponseContoller)->jsonError(false, 'Validation Error', 422, $validator->errors());
            }

            //Create a new user and store it in the database
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
            $token = $user->createToken('API TOKEN')->plainTextToken;
            $user->token = $token;

            return (new ResponseContoller)->jsonResponse(true, 'User Created Successfully', $user, 200);
        } catch (\Throwable $th) {
            //if has an server error or database error
            return (new ResponseContoller)->jsonError(false, $th->getMessage(), 500);
        }
    }

    public function loginUser(Request $request)
    {
        try {
            //Check if request fields validate
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required'
                ]
            );

            //Validate Fail
            if ($validator->fails()) {
                return (new ResponseContoller)->jsonError(false, 'Validation Error', 422, $validator->errors());
            }

            //Wrong Credential (Invalid Email or Password)
            if (!auth()->attempt($request->only(['email', 'password']))) {
                return (new ResponseContoller)->jsonError(false, 'Invalid Email or Password.', 401);
            }

            //Get the user data that has this email
            $user = User::where('email', $request->email)->first();
            $token = $user->createToken('API TOKEN')->plainTextToken;
            $user->token = $token;

            return (new ResponseContoller)->jsonResponse(true, 'User Logged In Successfully', $user, 200);
        } catch (\Throwable $th) {
            //if has an server error or database error
            return (new ResponseContoller)->jsonError(false, $th->getMessage(), 500);
        }
    }
}
