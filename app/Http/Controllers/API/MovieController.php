<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movie;
use App\Http\Resources\MovieBaseResource;
use App\Http\Controllers\ResponseContoller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;


class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get All Movies with related Genres (Order by id descending)
        $movies = MovieBaseResource::collection(Movie::with('genres')->orderBy('id', 'desc')->get());

        return (new ResponseContoller)->jsonResponse(true, '', $movies, 200, $movies->count());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Check if request fields validate
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'description' => 'required|string',
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                'rate' => 'required|numeric'
            ]
        );

        //Validate Fail
        if ($validator->fails()) {
            return (new ResponseContoller)->jsonError(false, 'Validation Error', 422, $validator->errors());
        }

        $fileName = time() . '-' . $request->file('image')->getClientOriginalName();
        $request->file('image')->storeAs('images', $fileName, 'public');

        //Create new Movie in database
        // $movie = Movie::create($request->all());
        $movie = Movie::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $fileName,
            'rate' => $request->rate,
        ]);

        if ($movie) { //if movie created successfully
            try {
                $movie->genres()->attach($request->genreIds); //Add genres related to the movie
                // $movie->genreIds = $request->genreIds;
                $movie->genres = $movie->genres;
            } catch (\Throwable $th) {
                return (new ResponseContoller)->jsonError(false, $th->getMessage(), 500);
            }
            $movie->image = URL::to('/') . Storage::url('images/' . $movie->image);
            return (new ResponseContoller)->jsonResponse(true, 'Movie Created Successfully', $movie, 201);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Something Went Wrong!', [], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = Movie::find($id); //Get the movie by its id

        if ($movie) {
            //if the movie exists
            $movie->genres = $movie->genres; //Get the genres related to this movie
            $movie->image = URL::to('/') . Storage::url('images/' . $movie->image);
            return (new ResponseContoller)->jsonResponse(true, '', $movie, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Movie Found!', [], 404);
        }
    }

    public function edit($id)
    {
        $movie = Movie::find($id); //Get the movie by its id

        if ($movie) {
            //if the movie exists
            $movie->genres = $movie->genres; //Get the genres related to this movie
            $movie->image = URL::to('/') . Storage::url('images/' . $movie->image);
            return (new ResponseContoller)->jsonResponse(true, '', $movie, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Movie Found!', [], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);

        //Check if request fields validate
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'description' => 'required|string',
                'rate' => 'required|numeric'
            ]
        );

        //Validate Fail
        if ($validator->fails()) {
            return (new ResponseContoller)->jsonError(false, 'Validation Error', 422, $validator->errors());
        }

        //find the movie by id
        $movie = Movie::find($id);

        if ($movie) {
            $movie->update([
                'name' => $request->name,
                'description' => $request->description,
                'rate' => $request->rate
            ]);
            $movie->genres = $movie->genres; //Get the genres related to this movie
            //Return json data to client view
            return (new ResponseContoller)->jsonResponse(true, 'Movie Updated Successfully', $movie, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Movie Found!', [], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);

        if ($movie) {
            $movie->image = URL::to('/') . Storage::url('images/' . $movie->image);
            $movie->genres()->detach();
            $movie->delete();
            return (new ResponseContoller)->jsonResponse(true, 'Movie Deleted Successfully', $movie, 200);
        } else {
            return (new ResponseContoller)->jsonResponse(false, 'Not Such Movie Found!', [], 404);
        }
    }

    public function searchbyName($name)
    {
        $movies = MovieBaseResource::collection(Movie::where('name', 'like', "%{$name}%")->get());
        return (new ResponseContoller)->jsonResponse(true, '', $movies, 200, $movies->count());
    }
}
