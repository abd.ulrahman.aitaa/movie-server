<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseContoller extends Controller
{
    public function jsonResponse($status, $message, $data, $statusCode, $count = null)
    {
        // dd($count);
        if ($count !== null) {
            return response()->json([
                'count' => $count,
                'status' => $status,
                'message' => $message,
                'data' => $data
            ], $statusCode);
        } else {
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $data
            ], $statusCode);
        }
    }

    public function jsonError($status, $message, $statusCode, $error = null)
    {
        // dd($error);
        if ($error !== null) {
            return response()->json([
                'status' => $status,
                'message' => $message,
                'errors' => $error
            ], $statusCode);
        } else {
            return response()->json([
                'status' => $status,
                'message' => $message,
            ], $statusCode);
        }
    }
}
