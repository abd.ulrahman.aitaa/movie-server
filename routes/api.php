<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\GenreController;
use App\Http\Controllers\API\MovieController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication APIs Route
Route::post('register', [AuthController::class, 'createUser']);
Route::post('login', [AuthController::class, 'loginUser']);

//Authenticated user (have valid access token)
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    // Genres APIs Route
    Route::get('genres', [GenreController::class, 'index']);
    Route::post('genres', [GenreController::class, 'store']);
    Route::get('genres/{id}', [GenreController::class, 'show']);
    Route::get('genres/{id}/edit', [GenreController::class, 'edit']);
    Route::put('genres/{id}/edit', [GenreController::class, 'update']);
    Route::delete('genres/{id}/delete', [GenreController::class, 'destroy']);

    // Movies APIs Route
    Route::get('movies', [MovieController::class, 'index']);
    Route::post('movies', [MovieController::class, 'store']);
    Route::get('movies/{id}', [MovieController::class, 'show']);
    Route::get('movies/{id}/edit', [MovieController::class, 'edit']);
    Route::put('movies/{id}/edit', [MovieController::class, 'update']);
    Route::delete('movies/{id}/delete', [MovieController::class, 'destroy']);
    Route::get('movies/search/{name}', [MovieController::class, 'searchByName']);
});
